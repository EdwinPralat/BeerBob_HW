BeerBob fermentation probe hardware project.

This project was created using KiCad release 5.0 and as such is incompatible with older versions of the 
software.

This project is a part of an OpenBrewery initiative (https://gitlab.com/OpenBrewery)
