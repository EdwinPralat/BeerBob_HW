update=Tue 15 May 2018 22:36:39 CEST
version=1
last_client=kicad
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[cvpcb]
version=1
NetIExt=net
[general]
version=1
[eeschema]
version=1
LibDir=../../kicad/kicad-symbols
[eeschema/libraries]
LibName1=/home/apralat/kicad/kicad-symbols/4xxx
LibName2=/home/apralat/kicad/kicad-symbols/4xxx_IEEE
LibName3=/home/apralat/kicad/kicad-symbols/74xGxx
LibName4=/home/apralat/kicad/kicad-symbols/74xx
LibName5=/home/apralat/kicad/kicad-symbols/74xx_IEEE
LibName6=/home/apralat/kicad/kicad-symbols/Amplifier_Audio
LibName7=/home/apralat/kicad/kicad-symbols/Amplifier_Buffer
LibName8=/home/apralat/kicad/kicad-symbols/Amplifier_Current
LibName9=/home/apralat/kicad/kicad-symbols/Amplifier_Difference
LibName10=/home/apralat/kicad/kicad-symbols/Amplifier_Instrumentation
LibName11=/home/apralat/kicad/kicad-symbols/Amplifier_Operational
LibName12=/home/apralat/kicad/kicad-symbols/Amplifier_Video
LibName13=/home/apralat/kicad/kicad-symbols/Analog
LibName14=/home/apralat/kicad/kicad-symbols/Analog_ADC
LibName15=/home/apralat/kicad/kicad-symbols/Analog_DAC
LibName16=/home/apralat/kicad/kicad-symbols/Analog_Switch
LibName17=/home/apralat/kicad/kicad-symbols/Audio
LibName18=/home/apralat/kicad/kicad-symbols/Battery_Management
LibName19=/home/apralat/kicad/kicad-symbols/Comparator
LibName20=/home/apralat/kicad/kicad-symbols/Connector
LibName21=/home/apralat/kicad/kicad-symbols/Connector_Generic
LibName22=/home/apralat/kicad/kicad-symbols/Connector_Generic_MountingPin
LibName23=/home/apralat/kicad/kicad-symbols/Connector_Generic_Shielded
LibName24=/home/apralat/kicad/kicad-symbols/Converter_ACDC
LibName25=/home/apralat/kicad/kicad-symbols/Converter_DCDC
LibName26=/home/apralat/kicad/kicad-symbols/CPLD_Altera
LibName27=/home/apralat/kicad/kicad-symbols/CPLD_Xilinx
LibName28=/home/apralat/kicad/kicad-symbols/CPU
LibName29=/home/apralat/kicad/kicad-symbols/CPU_NXP_6800
LibName30=/home/apralat/kicad/kicad-symbols/CPU_NXP_68000
LibName31=/home/apralat/kicad/kicad-symbols/CPU_PowerPC
LibName32=/home/apralat/kicad/kicad-symbols/Device
LibName33=/home/apralat/kicad/kicad-symbols/Diode
LibName34=/home/apralat/kicad/kicad-symbols/Diode_Bridge
LibName35=/home/apralat/kicad/kicad-symbols/Diode_Laser
LibName36=/home/apralat/kicad/kicad-symbols/Display_Character
LibName37=/home/apralat/kicad/kicad-symbols/Display_Graphic
LibName38=/home/apralat/kicad/kicad-symbols/Driver_Display
LibName39=/home/apralat/kicad/kicad-symbols/Driver_FET
LibName40=/home/apralat/kicad/kicad-symbols/Driver_LED
LibName41=/home/apralat/kicad/kicad-symbols/Driver_Motor
LibName42=/home/apralat/kicad/kicad-symbols/Driver_Relay
LibName43=/home/apralat/kicad/kicad-symbols/DSP_Freescale
LibName44=/home/apralat/kicad/kicad-symbols/DSP_Microchip_DSPIC33
LibName45=/home/apralat/kicad/kicad-symbols/DSP_Motorola
LibName46=/home/apralat/kicad/kicad-symbols/DSP_Texas
LibName47=/home/apralat/kicad/kicad-symbols/Filter
LibName48=/home/apralat/kicad/kicad-symbols/FPGA_Microsemi
LibName49=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx
LibName50=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Artix7
LibName51=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Kintex7
LibName52=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Spartan6
LibName53=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Virtex5
LibName54=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Virtex6
LibName55=/home/apralat/kicad/kicad-symbols/FPGA_Xilinx_Virtex7
LibName56=/home/apralat/kicad/kicad-symbols/GPU
LibName57=/home/apralat/kicad/kicad-symbols/Graphic
LibName58=/home/apralat/kicad/kicad-symbols/Interface
LibName59=/home/apralat/kicad/kicad-symbols/Interface_CAN_LIN
LibName60=/home/apralat/kicad/kicad-symbols/Interface_CurrentLoop
LibName61=/home/apralat/kicad/kicad-symbols/Interface_Ethernet
LibName62=/home/apralat/kicad/kicad-symbols/Interface_Expansion
LibName63=/home/apralat/kicad/kicad-symbols/Interface_HID
LibName64=/home/apralat/kicad/kicad-symbols/Interface_LineDriver
LibName65=/home/apralat/kicad/kicad-symbols/Interface_Optical
LibName66=/home/apralat/kicad/kicad-symbols/Interface_Telecom
LibName67=/home/apralat/kicad/kicad-symbols/Interface_UART
LibName68=/home/apralat/kicad/kicad-symbols/Interface_USB
LibName69=/home/apralat/kicad/kicad-symbols/Isolator
LibName70=/home/apralat/kicad/kicad-symbols/Isolator_Analog
LibName71=/home/apralat/kicad/kicad-symbols/Jumper
LibName72=/home/apralat/kicad/kicad-symbols/LED
LibName73=/home/apralat/kicad/kicad-symbols/Logic_LevelTranslator
LibName74=/home/apralat/kicad/kicad-symbols/Logic_Programmable
LibName75=/home/apralat/kicad/kicad-symbols/MCU_AnalogDevices
LibName76=/home/apralat/kicad/kicad-symbols/MCU_Cypress
LibName77=/home/apralat/kicad/kicad-symbols/MCU_Espressif
LibName78=/home/apralat/kicad/kicad-symbols/MCU_Intel
LibName79=/home/apralat/kicad/kicad-symbols/MCU_Microchip_8051
LibName80=/home/apralat/kicad/kicad-symbols/MCU_Microchip_ATmega
LibName81=/home/apralat/kicad/kicad-symbols/MCU_Microchip_ATtiny
LibName82=/home/apralat/kicad/kicad-symbols/MCU_Microchip_AVR
LibName83=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC10
LibName84=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC12
LibName85=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC16
LibName86=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC18
LibName87=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC24
LibName88=/home/apralat/kicad/kicad-symbols/MCU_Microchip_PIC32
LibName89=/home/apralat/kicad/kicad-symbols/MCU_Microchip_SAME
LibName90=/home/apralat/kicad/kicad-symbols/MCU_Microchip_SAML
LibName91=/home/apralat/kicad/kicad-symbols/MCU_Module
LibName92=/home/apralat/kicad/kicad-symbols/MCU_Nordic
LibName93=/home/apralat/kicad/kicad-symbols/MCU_NXP_ColdFire
LibName94=/home/apralat/kicad/kicad-symbols/MCU_NXP_HC11
LibName95=/home/apralat/kicad/kicad-symbols/MCU_NXP_HC12
LibName96=/home/apralat/kicad/kicad-symbols/MCU_NXP_HCS12
LibName97=/home/apralat/kicad/kicad-symbols/MCU_NXP_Kinetis
LibName98=/home/apralat/kicad/kicad-symbols/MCU_NXP_LPC
LibName99=/home/apralat/kicad/kicad-symbols/MCU_NXP_MAC7100
LibName100=/home/apralat/kicad/kicad-symbols/MCU_NXP_MCore
LibName101=/home/apralat/kicad/kicad-symbols/MCU_NXP_S08
LibName102=/home/apralat/kicad/kicad-symbols/MCU_Parallax
LibName103=/home/apralat/kicad/kicad-symbols/MCU_SiFive
LibName104=/home/apralat/kicad/kicad-symbols/MCU_SiliconLabs
LibName105=/home/apralat/kicad/kicad-symbols/MCU_ST_STM8
LibName106=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F0
LibName107=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F1
LibName108=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F2
LibName109=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F3
LibName110=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F4
LibName111=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32F7
LibName112=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32H7
LibName113=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32L0
LibName114=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32L1
LibName115=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32L4
LibName116=/home/apralat/kicad/kicad-symbols/MCU_ST_STM32L4+
LibName117=/home/apralat/kicad/kicad-symbols/MCU_Texas
LibName118=/home/apralat/kicad/kicad-symbols/MCU_Texas_MSP430
LibName119=/home/apralat/kicad/kicad-symbols/Mechanical
LibName120=/home/apralat/kicad/kicad-symbols/Memory_Controller
LibName121=/home/apralat/kicad/kicad-symbols/Memory_EEPROM
LibName122=/home/apralat/kicad/kicad-symbols/Memory_EPROM
LibName123=/home/apralat/kicad/kicad-symbols/Memory_Flash
LibName124=/home/apralat/kicad/kicad-symbols/Memory_NVRAM
LibName125=/home/apralat/kicad/kicad-symbols/Memory_RAM
LibName126=/home/apralat/kicad/kicad-symbols/Memory_ROM
LibName127=/home/apralat/kicad/kicad-symbols/Memory_UniqueID
LibName128=/home/apralat/kicad/kicad-symbols/Motor
LibName129=/home/apralat/kicad/kicad-symbols/Oscillator
LibName130=/home/apralat/kicad/kicad-symbols/Potentiometer_Digital
LibName131=/home/apralat/kicad/kicad-symbols/power
LibName132=/home/apralat/kicad/kicad-symbols/Power_Management
LibName133=/home/apralat/kicad/kicad-symbols/Power_Protection
LibName134=/home/apralat/kicad/kicad-symbols/Power_Supervisor
LibName135=/home/apralat/kicad/kicad-symbols/pspice
LibName136=/home/apralat/kicad/kicad-symbols/Reference_Current
LibName137=/home/apralat/kicad/kicad-symbols/Reference_Voltage
LibName138=/home/apralat/kicad/kicad-symbols/Regulator_Controller
LibName139=/home/apralat/kicad/kicad-symbols/Regulator_Current
LibName140=/home/apralat/kicad/kicad-symbols/Regulator_Linear
LibName141=/home/apralat/kicad/kicad-symbols/Regulator_SwitchedCapacitor
LibName142=/home/apralat/kicad/kicad-symbols/Regulator_Switching
LibName143=/home/apralat/kicad/kicad-symbols/Relay
LibName144=/home/apralat/kicad/kicad-symbols/Relay_SolidState
LibName145=/home/apralat/kicad/kicad-symbols/RF
LibName146=/home/apralat/kicad/kicad-symbols/RF_AM_FM
LibName147=/home/apralat/kicad/kicad-symbols/RF_Amplifier
LibName148=/home/apralat/kicad/kicad-symbols/RF_Bluetooth
LibName149=/home/apralat/kicad/kicad-symbols/RF_GPS
LibName150=/home/apralat/kicad/kicad-symbols/RF_Mixer
LibName151=/home/apralat/kicad/kicad-symbols/RF_Module
LibName152=/home/apralat/kicad/kicad-symbols/RF_RFID
LibName153=/home/apralat/kicad/kicad-symbols/RF_Switch
LibName154=/home/apralat/kicad/kicad-symbols/RF_WiFi
LibName155=/home/apralat/kicad/kicad-symbols/RF_ZigBee
LibName156=/home/apralat/kicad/kicad-symbols/Sensor
LibName157=/home/apralat/kicad/kicad-symbols/Sensor_Audio
LibName158=/home/apralat/kicad/kicad-symbols/Sensor_Current
LibName159=/home/apralat/kicad/kicad-symbols/Sensor_Gas
LibName160=/home/apralat/kicad/kicad-symbols/Sensor_Humidity
LibName161=/home/apralat/kicad/kicad-symbols/Sensor_Magnetic
LibName162=/home/apralat/kicad/kicad-symbols/Sensor_Motion
LibName163=/home/apralat/kicad/kicad-symbols/Sensor_Optical
LibName164=/home/apralat/kicad/kicad-symbols/Sensor_Pressure
LibName165=/home/apralat/kicad/kicad-symbols/Sensor_Proximity
LibName166=/home/apralat/kicad/kicad-symbols/Sensor_Temperature
LibName167=/home/apralat/kicad/kicad-symbols/Sensor_Touch
LibName168=/home/apralat/kicad/kicad-symbols/Sensor_Voltage
LibName169=/home/apralat/kicad/kicad-symbols/Switch
LibName170=/home/apralat/kicad/kicad-symbols/Timer
LibName171=/home/apralat/kicad/kicad-symbols/Timer_PLL
LibName172=/home/apralat/kicad/kicad-symbols/Timer_RTC
LibName173=/home/apralat/kicad/kicad-symbols/Transformer
LibName174=/home/apralat/kicad/kicad-symbols/Transistor_Array
LibName175=/home/apralat/kicad/kicad-symbols/Transistor_BJT
LibName176=/home/apralat/kicad/kicad-symbols/Transistor_FET
LibName177=/home/apralat/kicad/kicad-symbols/Transistor_IGBT
LibName178=/home/apralat/kicad/kicad-symbols/Triac_Thyristor
LibName179=/home/apralat/kicad/kicad-symbols/Valve
LibName180=/home/apralat/kicad/kicad-symbols/Video
LibName181=libraries/beerbob_symbols/beerbob
[schematic_editor]
version=1
PageLayoutDescrFile=
PlotDirectoryName=
SubpartIdSeparator=0
SubpartFirstId=65
NetFmtName=
SpiceForceRefPrefix=0
SpiceUseNetNumbers=0
LabSize=60
